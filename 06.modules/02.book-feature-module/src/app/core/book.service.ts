import { Injectable } from '@angular/core';
import { Book } from './model/book';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  books: Book[] = [
    {
      isbn: 'abc',
      title: 'My first book',
      description: 'Description of my first book',
      authors: 'Author A'
    },
    {
      isbn: 'def',
      title: 'My Second book',
      description: 'Description of my second book'
    }
  ];

  constructor() { }


  getAllBooks(): Observable<Book[]> {
    return of(this.books);
  }

  deletBook(isbn: string) {
    this.books = this.books.filter(book => book.isbn !== isbn);
  }
}
