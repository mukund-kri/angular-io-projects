import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonModule } from 'primeng/button';
import { MenubarModule } from 'primeng/menubar';
import { DataViewModule } from 'primeng/dataview';

import { BooksRoutingModule } from './books-routing.module';
import { BookListComponent } from './book-list/book-list.component';
import { BookCreateComponent } from './book-create/book-create.component';
import { BookListItemComponent } from './book-list-item/book-list-item.component';
import { BookEditComponent } from './book-edit/book-edit.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { BookRootComponent } from './book-root/book-root.component';


@NgModule({
  declarations: [
    BookListComponent,
    BookCreateComponent,
    BookListItemComponent,
    BookEditComponent,
    BookDetailsComponent,
    BookRootComponent
  ],
  imports: [
    CommonModule,
    BooksRoutingModule,

    ButtonModule,
    MenubarModule,
    DataViewModule,
    
  ]
})
export class BooksModule { }
