import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookListComponent } from './book-list/book-list.component';
import { BookCreateComponent } from './book-create/book-create.component';
import { BookEditComponent } from './book-edit/book-edit.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { BookRootComponent } from './book-root/book-root.component';


const routes: Routes = [
  { path: '',
    component: BookRootComponent,
    children: [
      { path: '', component: BookListComponent },
      { path: 'create', component: BookCreateComponent },
      { path: 'edit/:isbn', component: BookEditComponent },
      { path: 'details/:isbn', component: BookDetailsComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BooksRoutingModule { }
