import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/core/book.service';
import { Book } from 'src/app/core/model/book';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.less']
})
export class BookListComponent implements OnInit {

  private books$: Observable<Book[]>;

  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.books$ = this.bookService.getAllBooks();
  }

}
