import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/components/common/menuitem';
import { timingSafeEqual } from 'crypto';

@Component({
  selector: 'app-book-root',
  templateUrl: './book-root.component.html',
  styleUrls: ['./book-root.component.less']
})
export class BookRootComponent implements OnInit {

  items: MenuItem[];

  constructor() { }

  ngOnInit() {
    this.items = [
      {
        label: 'Books Home',
        routerLink: ['/books']
      },
      {
        label: 'Add Book',
        icon: 'pi pi-fw pi-pencil',
        routerLink: ['/books/create']
      }
    ];
  }

}
