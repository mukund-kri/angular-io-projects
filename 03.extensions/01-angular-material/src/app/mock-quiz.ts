import { Quiz, MultipleChoice } from './models';


export const mockQuiz: Quiz = {
  description: "This is a dummy quiz",
  instruction: "These are dummy instructions",

  questions: [
    {
      question: "What is the capital of India?",
      options: ["Bangalore", "Delhi", "Chenni", "Mumbai"],

      answers: [1]
    },
    {
      question: "What is the IT capital of India?",
      options: ["Bangalore", "Delhi", "Chenni", "Mumbai"],

      answers: [0]
    }

  ]

}
