# Tasks while creating project

1. Create angular project with angular cli. Install dependencies.
2. Add router to this project.
2. Add model to represent book. (can be class of interface)
3. Add a service to this project and code a data structure to hold the books (can list / map)
4. Add the following components. (all components are compulsory)
    - list books
    - list books item
    - create book 
    - update book 
    - book detail
5. list books component: takes care of listing all books
6. list books item component: takes care of displaying and behavior of 
individual books.
7. create book component: form to create a book 
8. update book component: form to update a book 
9. book detail component: Display all the details of an individual book 

