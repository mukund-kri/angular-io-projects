# Quiz

## An intermediate level angular app. A Quiz app.

This particular application aims to tackle on the UI portion. In addition
this is the initial attempt and every thing is done the easy way. Subsequent 
project will deal with more complex issues of state management, server access,
graphql and much more.


## Docs from angular cli
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.4.

Since this was generated with angular-cli all the ng commands will work. With 
this project. Refer the documentation of angular-cli for that documentation.
