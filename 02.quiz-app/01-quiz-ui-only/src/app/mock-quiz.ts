import { Quiz, MultipleChoice } from './models';


export const quiz: Quiz = {

  description: "Mock Quiz",
  instructions: "Answer all the questions",

  questions: [
    {
      question: "What is the capital of India?",
      options: ["Bangalore", "Delhi", "Mumbai", "Chennai"],
      answers: [1]

    }
  ]
};
