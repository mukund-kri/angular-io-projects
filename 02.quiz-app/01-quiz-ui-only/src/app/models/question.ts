export class Question {
  qType: string;

  qText: string;
}


export class MultipleChoiceQuestion {
  qType = 'multiple_choice';

  distractors: string[];
  correct_options: string[];
}

export class SingleChoiceQuestion extends Question {
  qType = 'single_choice';

  distractors: string[];
  answer: string;
}
