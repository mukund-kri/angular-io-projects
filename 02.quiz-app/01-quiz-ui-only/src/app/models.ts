

export type MultipleChoice = {
  question: string,
  options: string[],
  answers: number[]
}


export type Quiz = {
  description: string,
  instructions: string,

  questions: MultipleChoice[]
}
