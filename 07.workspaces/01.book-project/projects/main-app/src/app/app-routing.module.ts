import { NgModule } from '@angular/core';
import { RouterModule, Router, Routes } from '@angular/router';


const routes: Routes = [
  { path: '', loadChildren: './books/books.module#BooksModule' },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
