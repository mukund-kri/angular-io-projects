import { Component, OnInit } from '@angular/core';
import { BooksApiClientService } from 'books-api-client';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  constructor(private booksSevice: BooksApiClientService) { }

  ngOnInit() {
    this.booksSevice.getBooks()
    .subscribe(books => console.log(books));
  }


}
