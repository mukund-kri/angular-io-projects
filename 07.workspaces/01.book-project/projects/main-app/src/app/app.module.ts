import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BooksApiClientModule } from 'books-api-client';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BooksApiClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
