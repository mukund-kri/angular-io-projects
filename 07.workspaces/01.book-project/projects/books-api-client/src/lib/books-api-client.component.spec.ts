import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BooksApiClientComponent } from './books-api-client.component';

describe('BooksApiClientComponent', () => {
  let component: BooksApiClientComponent;
  let fixture: ComponentFixture<BooksApiClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BooksApiClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BooksApiClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
