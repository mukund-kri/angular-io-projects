import { NgModule } from '@angular/core';
import { BooksApiClientComponent } from './books-api-client.component';
import { BooksApiClientService } from 'books-api-client/public_api';

@NgModule({
  declarations: [BooksApiClientComponent, BooksApiClientService],
  imports: [
  ],
  exports: [BooksApiClientComponent, BooksApiClientService]
})
export class BooksApiClientModule { }
