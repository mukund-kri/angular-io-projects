import { Injectable } from '@angular/core';
import { Book } from '../models/book.model';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksApiClientService {

  public data: Map<string, Book> = new Map([
    [ 'abc',
      { isbn: 'abc',
        title: 'My first book',
        description: 'Description of my first book',
        authors: 'Author A'
      }
    ]
  ]);

  constructor() { }

  getBooks(): Observable<Map<string, Book>> {
    return of(this.data);
  }
}
