# All the gory details of angular projects


## At the top sits a workspace

## A workspace has many projects

A project can be a 
1. app, 
2. app's e2e testing, 
3. library 
4. ...

## A project can have a lot of modules

Modules can be a
1. Feature module
2. Router module,
and many more that I have not explored.

## Modules are made up of Components, Directives, Pipes etc.


