# Only Book UI

## INTRO
This is the basic version of a frontend app written in angular.io. This 
is built to work in conjunction with the Django API available here at [1]

## TECHNOLOGY - ANGULAR CLI
This project was generated with [Angular CLI].
Refer [Angular Cli section](ANGULAR-CLI.md) to run this app.

## TECHNOLOGY - DJANGO

This project need REST API from this [1] project. Please refer the readme that 
comes with that project to run the django server. 

## REFERENCES

[1] https://bitbucket.org/mukund_kri1/django-postgres-rest-examples/src/master/simple/02.only.book.api/
