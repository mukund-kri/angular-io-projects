export class Book {
  isbn: string;
  title: string;
  description: string;
  authors: string;
}
