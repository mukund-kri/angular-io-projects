import { Component, OnInit, Input } from '@angular/core';
import { BookService } from '../book.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Book } from '../models/book';

@Component({
  selector: 'app-book-update',
  templateUrl: './book-update.component.html',
  styleUrls: ['./book-update.component.css']
})
export class BookUpdateComponent implements OnInit {

  private book: Book;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private bookService: BookService) { }

  ngOnInit() {
    const isbn: string = this.route.snapshot.params.isbn;
    this.bookService.getBookById(isbn)
      .subscribe(book => this.book = book);
  }

  onSubmit() {
    this.bookService.updateBook(this.book)
      .subscribe( _ => this.router.navigate(['/']));
  }
}
