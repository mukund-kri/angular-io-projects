import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book } from '../models/book';
import { BookService } from '../book.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-book-list-item',
  templateUrl: './book-list-item.component.html',
  styleUrls: ['./book-list-item.component.css']
})
export class BookListItemComponent implements OnInit {

  @Input() book: Book;
  @Output() deleteBook = new EventEmitter<Book>();

  constructor(private bookService: BookService) { }

  ngOnInit() {
  }

  deleteThisBook() {
    this.deleteBook.emit(this.book);
  }
}
