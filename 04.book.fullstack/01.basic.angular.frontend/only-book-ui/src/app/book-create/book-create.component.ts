import { Component, OnInit } from '@angular/core';

import { Book } from '../models/book';
import { BookService } from '../book.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.css']
})
export class BookCreateComponent {

  private book: Book = new Book();

  constructor(
    private router: Router,
    private bookSerivice: BookService) { }

  onSubmit() {
    this.bookSerivice.createBook(this.book)
      .subscribe( _ => this.router.navigate(['/']));
  }
}
