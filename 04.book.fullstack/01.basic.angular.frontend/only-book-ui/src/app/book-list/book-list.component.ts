import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import { Book } from '../models/book';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  private books: Book[] = [];

  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.populateBooks();
  }

  deleteBook(book: Book) {
    this.bookService.deleteBook(book.isbn)
    .subscribe(json => {
      this.populateBooks();
    });
  }

  populateBooks() {
    this.bookService.getBooks()
      .subscribe((books) => this.books = books);
  }
}
