import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import { Router } from '@angular/router';
import { Book } from '../models/book.model';

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.less']
})
export class BookCreateComponent {

  private book: Book = { title: '', description: '', authors: ''};

  constructor(
    private bookService: BookService,
    private router: Router
  ) { }

  onSubmit() {
    this.bookService.createBook(this.book)
    .subscribe(_ => this.router.navigate(['/']));
  }

  resetForm() {
    this.book = { title: '', description: '', authors: ''};
  }
}
