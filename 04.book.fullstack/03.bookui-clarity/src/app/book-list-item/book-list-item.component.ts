import { Component, OnInit, Input } from '@angular/core';
import { BookService } from '../book.service';
import { Book } from '../models/book.model';
import { Router } from '@angular/router';

@Component({
  selector: '[app-book-list-item]',
  templateUrl: './book-list-item.component.html',
  styleUrls: ['./book-list-item.component.less']
})
export class BookListItemComponent {

  @Input() book: Book;

  constructor(
    private bookService: BookService,
    private router: Router
  ) { }


  deleteBook() {
    this.bookService.deleteBook(this.book.id)
    .subscribe(_ => this.router.navigate(['/']));
  }

  showDetails() {
    this.router.navigate(['details', this.book.id]);
  }
}
