import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Book } from './models/book.model';


const SERVER_URL = 'http://localhost:8080/book/';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }

  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(`${SERVER_URL}`);
  }

  getBookById(id: number): Observable<Book> {
    return this.http.get<Book>(`${SERVER_URL}${id}`);
  }

  deleteBook(id: number) {
    return this.http.delete(`${SERVER_URL}${id}`);
  }

  createBook(book: Book): Observable<Book> {
    return this.http.post<Book>(
      `${SERVER_URL}`,
      book
    );
  }

  updateBook(book: Book): Observable<Book> {
    return this.http.put<Book>(`${SERVER_URL}${book.id}`, book);
  }
}
