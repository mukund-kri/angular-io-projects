import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '../models/book.model';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.less']
})
export class BookEditComponent implements OnInit {

  private book: Book;

  constructor(
    private bookService: BookService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];
    this.bookService.getBookById(id)
    .subscribe(book => this.book = book);
  }

  updateBook() {
    this.bookService.updateBook(this.book)
    .subscribe(_ => this.router.navigate(['/']));
  }
}
