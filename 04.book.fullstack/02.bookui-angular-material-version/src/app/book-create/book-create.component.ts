import { Component, OnInit } from '@angular/core';
import { Book } from '../models/book';
import { Router } from '@angular/router';
import { BookService } from '../book.service';

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.less']
})
export class BookCreateComponent implements OnInit {

  private book: Book = {
    isbn: '',
    title: '',
    description: '',
    authors: '',
  };

  constructor(
    private router: Router,
    private bookService: BookService,
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.bookService.createBook(this.book)
      .subscribe(_ => this.router.navigate(['/']));
  }
}
