import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book } from '../models/book';
import { Router, RouteReuseStrategy } from '@angular/router';

@Component({
  selector: 'app-book-list-item',
  templateUrl: './book-list-item.component.html',
  styleUrls: ['./book-list-item.component.less']
})
export class BookListItemComponent {

  @Input() private book: Book;
  @Output() private deleteBook: EventEmitter<Book> = new EventEmitter();

  constructor(private router: Router) { }

  deleteThisBook() {
    this.deleteBook.emit(this.book);
  }

  navigateToDetails() {
    this.router.navigate(['/details', this.book.isbn]);
  }

}
