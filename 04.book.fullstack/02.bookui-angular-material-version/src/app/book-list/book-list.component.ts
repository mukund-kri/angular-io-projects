import { Component, OnInit } from '@angular/core';
import { Book } from '../models/book';
import { BookService } from '../book.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.less']
})
export class BookListComponent implements OnInit {

  private books: Book[] = [];

  constructor(private bookService: BookService) { }

  ngOnInit() {
    this.populateBooks();
  }

  deleteBook(book: Book) {
    this.bookService.deleteBook(book.isbn)
    .subscribe(json => {
      this.populateBooks();
    });
  }

  populateBooks() {
    this.bookService.getBooks()
      .subscribe(books => this.books = books);
  }
}
