import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Book } from './models/book';


const SERVER_URL = 'http://localhost:8080/book/';


@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) { }

  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(`${SERVER_URL}`);
  }

  getBookById(isbn: string): Observable<Book> {
    return this.http.get<Book>(`${SERVER_URL}${isbn}/`);
  }

  deleteBook(isbn: string) {
    return this.http.delete(`${SERVER_URL}${isbn}/`);
  }

  createBook(book: Book): Observable<Book> {
    return this.http.post<Book>(
      `${SERVER_URL}`,
      book
    );
  }

  updateBook(book: Book): Observable<Book> {
    return this.http.put<Book>(`${SERVER_URL}${book.isbn}/`, book);
  }
}
