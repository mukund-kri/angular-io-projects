import { Component, OnInit } from '@angular/core';
import { Book } from '../models/book';
import { ActivatedRoute, Router } from '@angular/router';
import { BookService } from '../book.service';

@Component({
  selector: 'app-book-update',
  templateUrl: './book-update.component.html',
  styleUrls: ['./book-update.component.less']
})
export class BookUpdateComponent implements OnInit {

  private book: Book;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private bookService: BookService
  ) { }

  ngOnInit() {
    const isbn = this.route.snapshot.paramMap.get('isbn');
    this.bookService.getBookById(isbn)
      .subscribe(book => this.book = book);
  }

  onSubmit() {
    this.bookService.updateBook(this.book)
      .subscribe(_ => this.router.navigate(['/']));
  }

}
