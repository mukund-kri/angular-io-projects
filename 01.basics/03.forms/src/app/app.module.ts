import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TemplateFormsComponent } from './template-forms/template-forms.component';
import { AppRoutingModule } from './app-routing.module';
import { IndexComponent } from './index/index.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { FormGroupExampleComponent } from './form-group-example/form-group-example.component';
import { SingleControlReactiveFormComponent } from './single-control-reactive-form/single-control-reactive-form.component';
import { FormBuilderExampleComponent } from './form-builder-example/form-builder-example.component';


@NgModule({
  declarations: [
    AppComponent,
    TemplateFormsComponent,
    IndexComponent,
    ReactiveFormComponent,
    FormGroupExampleComponent,
    SingleControlReactiveFormComponent,
    FormBuilderExampleComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
<<<<<<< HEAD
    AppRoutingModule
=======
    AppRoutingModule,
>>>>>>> e347ffcd849fcbf99b16c1e29b8a450e012d66c2
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
