import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-single-control-reactive-form',
  templateUrl: './single-control-reactive-form.component.html',
  styleUrls: ['./single-control-reactive-form.component.css']
})
export class SingleControlReactiveFormComponent {

  private title: FormControl = new FormControl('');

  constructor() { }

}
