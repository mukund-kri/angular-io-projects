import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleControlReactiveFormComponent } from './single-control-reactive-form.component';

describe('SingleControlReactiveFormComponent', () => {
  let component: SingleControlReactiveFormComponent;
  let fixture: ComponentFixture<SingleControlReactiveFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleControlReactiveFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleControlReactiveFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
