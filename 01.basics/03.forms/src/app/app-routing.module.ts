import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TemplateFormsComponent } from './template-forms/template-forms.component';
import { IndexComponent } from './index/index.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { SingleControlReactiveFormComponent } from './single-control-reactive-form/single-control-reactive-form.component';
import { FormGroupExampleComponent } from './form-group-example/form-group-example.component';
import { FormBuilderExampleComponent } from './form-builder-example/form-builder-example.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'template-form', component: TemplateFormsComponent },
  { path: 'reactive-form', component: ReactiveFormComponent },

  { path: 'simple-reactive', component: SingleControlReactiveFormComponent },
  { path: 'form-group', component: FormGroupExampleComponent },
  { path: 'form-builder', component: FormBuilderExampleComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
