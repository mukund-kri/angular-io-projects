import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-form-group-example',
  templateUrl: './form-group-example.component.html',
  styleUrls: ['./form-group-example.component.css']
})
export class FormGroupExampleComponent {

  private bookForm = new FormGroup({
    isbn: new FormControl(''),
    title: new FormControl(''),
    description: new FormControl(''),
  })

  constructor() { }

  onSubmit() {
    console.log(this.bookForm.value);
  }

}
