
import { Component, OnInit } from '@angular/core';

import { Hero } from '../models/Hero';


@Component({
  selector: 'app-template-forms',
  templateUrl: './template-forms.component.html',
  styleUrls: ['./template-forms.component.css']
})
export class TemplateFormsComponent {

  powers = ["Really Smart", "Super Flexible", "Super Hot", "Weather Changer"];

  constructor() { }

  model = new Hero(18, "Mr. IQ", this.powers[0], "Chuck Streeter");

  submitted = false;

  onSubmit() {
    this.submitted = true;
    console.log(this.model);
  }

  diagnostic(): string {
    return JSON.stringify(this.model);
  }

  newHero() {
    this.model = new Hero(19, '' , '')
  }
}
