import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-form-builder-example',
  templateUrl: './form-builder-example.component.html',
  styleUrls: ['./form-builder-example.component.css']
})
export class FormBuilderExampleComponent {

  private bookForm = this.fb.group({
    ISBN: [''],
    title: [''],
    description: [''],
    author: this.fb.group({
      firstName: [''],
      lastName: [''],
    }),
  });

  constructor(private fb: FormBuilder) { }

  onSubmit() {
    console.log(JSON.stringify(this.bookForm.value));
  }
}
