import { Injectable } from '@angular/core';
import { Book } from './models/book';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  public books: Map<string, Book> = new Map();

  constructor() {
    this.books.set("abcd", new Book("abcd", "Book A", "A nice book", "Author A"));
  }
}
