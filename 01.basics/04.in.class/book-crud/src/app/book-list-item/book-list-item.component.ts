import { Component, OnInit, Input } from '@angular/core';
import { Book } from '../models/book';
import { BookService } from '../book.service';

@Component({
  selector: 'app-book-list-item',
  templateUrl: './book-list-item.component.html',
  styleUrls: ['./book-list-item.component.css']
})
export class BookListItemComponent implements OnInit {

  @Input() private book: Book;

  constructor(private bookService: BookService) { }

  ngOnInit() {
  }

  delete() {
    this.bookService.books.delete(this.book.isbn);
  }
}
