import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '../models/book';
import { BookService } from '../book.service';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {

  private book: Book;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private bookSerivce: BookService) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.book = this.bookSerivce.books.get(params.id);
    });
  }

  updateBook() {
    this.bookSerivce.books.set(this.book.isbn, this.book);
    this.router.navigate(['/']);
  }

}
