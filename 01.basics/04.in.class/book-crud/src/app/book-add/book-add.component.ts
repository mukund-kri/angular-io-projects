import { Component, OnInit } from '@angular/core';
import { Book } from '../models/book';
import { BookService } from '../book.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-add',
  templateUrl: './book-add.component.html',
  styleUrls: ['./book-add.component.css']
})
export class BookAddComponent implements OnInit {

  private book: Book = new Book('', '', '', '');

  constructor(
    private router: Router,
    private bookService: BookService,
    ) { }

  ngOnInit() {
  }

  addBook() {
    this.bookService.books.set(this.book.isbn, this.book);
    // this.router.navigate(['/']);
  }
}
