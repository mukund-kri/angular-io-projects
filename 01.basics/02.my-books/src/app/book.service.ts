import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Book } from '../models/book';
import { books } from '../models/mock-books';


@Injectable({
  providedIn: 'root'
})
export class BookService {

  public books: Map<string, Book> = new Map();

  getBooks(): Observable<Book[]> {
    return of(books);
  }

  addBook(book: Book) {
    this.books.set(book.isbn, book);
  }

  deleteBook(isbn: string) {
    this.books.delete(isbn);
  }

  constructor() {
    for(let book of books) {
      this.books.set(book.isbn, book);
    }
  }
}
