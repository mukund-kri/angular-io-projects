import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';

import { Book } from '../../models/book';


@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent {

  private book: Book = {isbn: '', name: ''}

  constructor(
    private bookService: BookService
  ) { }

  onSubmit() {
    this.bookService.addBook(this.book);
    this.book = {isbn: '', name: ''};
  }

}
