import { Component, OnInit } from '@angular/core';

import { Book } from '../../models/book';
import { BookService } from '../book.service';


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  constructor(private bookService: BookService) { }

  ngOnInit() {
    console.log(this.bookService.books);
  }

  deleteBook(isbn: string) {
    this.bookService.deleteBook(isbn);
  }
}
