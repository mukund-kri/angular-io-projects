import { Book } from './book';

export const books: Book[] = [
  { isbn: '007246464X', name: 'Biology' },
  { isbn: '0072465239', name: 'Electric Machinery Fundamentals' }
]

/*
0072465638: Database Management Systems
0072466855: Introduction To The Finite Element Method
0072467509: Introduction to Computing Systems: From Bits and Gates to C and Beyond
0072468246: Methods, Standards, and Work Design
007246836X: Introduction to Probability and Statistics: Principles and Applications for Engineering and the Computing S
0072471468: Product Design and Development
0072478527: Basic Econometrics
0072492953: Music: An Appreciation
0072499389: Fundamentals of Digital Logic with VHDL Design
0072505036: Microelectronic Circuit Design
0072525231: Basic Marketing: A Global-Managerial Approach
0072528303: Vertebrates: Comparative Anatomy, Function, Evolution
0072534958: Physical Chemistry */
