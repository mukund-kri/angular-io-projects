import {
  Rule,
  SchematicContext,
  Tree,
} from '@angular-devkit/schematics';

// import { getProject, buildDefaultPath } from '@angular/cli/utilities/project/';

import { getWorkspace } from '@schematics/angular/utility/config';
import { InlineComponentOptions } from './schema';



export function rgComponent(options: InlineComponentOptions): Rule {
  return (host: Tree, _context: SchematicContext) => {

    const workspace = getWorkspace(host);
 
    if (!options.project) {
      options.project = Object.keys(workspace.projects)[0];
    }
 
    const project = workspace.projects[options.project];

    return host;
  }
}
