export interface InlineComponentOptions {
  path: string;
  project: string;
  name: string;
  module: string;
}
