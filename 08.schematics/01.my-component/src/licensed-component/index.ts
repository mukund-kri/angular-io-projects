import {
  Rule,
  Tree,
  chain,
  externalSchematic,
  SchematicContext,
} from '@angular-devkit/schematics';


const license = `
/*
@license
copyleft

Do any thing you want with this code.
*/
`;

export function licensedComponent(_options: any): Rule {
  
  return chain([
    externalSchematic('@schematics/angular', 'component', _options),
    (tree: Tree, _context: SchematicContext) => {

      tree.getDir('./src/')
        .visit(filePath => {

          if(!filePath.endsWith('.ts')) {
            return;
          };

          const content = tree.read(filePath);
          if(!content) {
            return;
          };

          if (content.indexOf(license) == -1) {
            tree.overwrite(filePath, license + content);
          };
        });
      return tree;
    },
  ])
}
