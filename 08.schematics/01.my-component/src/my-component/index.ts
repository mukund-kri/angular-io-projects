import {
  Rule,
  SchematicContext,
  Tree,
  apply,
  url,
  template,
  mergeWith,
  MergeStrategy,
} from '@angular-devkit/schematics';
import { strings } from '@angular-devkit/core';


export function myComponent(_options: any): Rule {
    return (tree: Tree, _context: SchematicContext) => {

      const templateSource = apply(url('./files'), [
        template({
          ...strings,
          ..._options,
        })
      ]);
      
      const rule = mergeWith(templateSource, MergeStrategy.Default);

      return rule(tree, _context);
  };
}
