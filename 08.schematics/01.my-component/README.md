# My First Schematic 

This project largely follow this article. 

https://blog.angular.io/schematics-an-introduction-dc1dfbc2a2b2

In this project I explore the very basics of schematics. This version only
copies a file `hello.ts` from the files folder to the current project.

## Concepts

The concepts that I gleaned from this project are ...

### Simplest Template
1. A first look at the schematics cli 
2. generate a blank schematics. explore the essentials of a schematic.
3. The `src/collection.json` file which lists and describes all the schematics
in this project.
4. The `src/<schematic>/index.ts` which contains the `RuleFactory` that drives
this schematic.
5. The rule factory returns a `Rule`. A `Rule` is a function that takes in a
`Tree` representing the current state of the project and returns another `Tree`
that represents the modified state of the project.

### Chaining components

1. Adding multiple schematics into a project.
  1. Add entry into `src/collection.json`, schematics section.
  2. Create as new folder in `src` for the new schematics code and add an 
  index.ts file.
  3. Code the `RuleFactory` inside the new `index.ts`
2. Using a proper workflow, using npm link to separate out the schematic from
the angular project.
2. using `npm run build -- -w` to continuously build the schematic project.
2. Chaining schematics with the chain function
3. Overwriting an existing file with new content


## Retrospective

1. I learnt the very basics of Angular Schematics.
2. Now I can get into more advanced use cases with schematics
