# Book App :: Module version

Continuing the theme of Book App, this one will build up to a clone
of http://goodreads.com. In this version we'll explore how to 
split up our component into feature modules. 

Here we have 3 feature modules, for ...
1. The admin section where books and authors are added/deleted/updated
2. The public section where users can browse the books
3. The user login where the user can rate the book and leave comments
   on the book.

   