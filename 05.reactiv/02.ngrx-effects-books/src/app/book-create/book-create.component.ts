import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import { Router } from '@angular/router';
import { Book } from '../models/book.model';
import { Store } from '@ngrx/store';
import { AppState } from '../store/reducers';
import { AddBookRequestAction } from '../store/actions';

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html'
})
export class BookCreateComponent {

  private book: Book = { title: '', description: '', authors: ''};

  constructor(
    private bookService: BookService,
    private router: Router,
    private store: Store<AppState>,
  ) { }

  onSubmit() {
    // TODO: Should redirect after success of Http post
    this.store.dispatch(new AddBookRequestAction(this.book));
    this.router.navigate(['/']);
  }

  resetForm() {
    this.book = { title: '', description: '', authors: ''};
  }
}
