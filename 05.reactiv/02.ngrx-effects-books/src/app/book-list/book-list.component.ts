import { Component, OnInit } from '@angular/core';
import { Book } from '../models/book.model';
import { BookService } from '../book.service';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState, bookMap } from '../store/reducers';
import * as actions from '../store/actions';


@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html'
})
export class BookListComponent implements OnInit {
  private books$: Observable<Map<number, Book>>;

  constructor(
    private bookService: BookService,
    private store: Store<AppState>
    ) {}

  ngOnInit() {
    this.store.dispatch(new actions.LoadBooksRequestAction());
    this.books$ = this.store.select(bookMap);
  }
}
