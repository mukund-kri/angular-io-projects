import { Action } from '@ngrx/store';
import { Book } from 'src/app/models/book.model';


export enum BookActionTypes {
    LOAD_BOOKS_REQUEST = '[Books] Load Request',
    LOAD_BOOKS_FAILURE = '[Books] Load Failure',
    LOAD_BOOKS_SUCCESS = '[Books] Load Success',

    SELECT_BOOK = '[Book] Select Book',
    DELETE_BOOK_REQUEST = '[Book] Delete Book Request',
    DELETE_BOOK_SUCCESS = '[Book] Delete Book Success',

    ADD_BOOK_REQUEST = '[Book] Add Book Request',
    ADD_BOOK_SUCCESS = '[Book] Add Book Success',

    UPDATE_BOOK_REQUEST = '[Book] Update Book Request',
    UPDATE_BOOK_SUCCESS = '[Book] Update Book Success',
}

export class LoadBooksRequestAction implements Action {
    readonly type = BookActionTypes.LOAD_BOOKS_REQUEST;

    constructor() {}
}

export class LoadBooksFailureAction implements Action {
    readonly type = BookActionTypes.LOAD_BOOKS_FAILURE;

    constructor(
        readonly payload: any,
    ) {}
}

export class LoadBooksSuccessAction implements Action {
    readonly type = BookActionTypes.LOAD_BOOKS_SUCCESS;

    constructor(
        readonly payload: Book[],
    ) {}
}

export class SelectBookAction implements Action {
    readonly type = BookActionTypes.SELECT_BOOK;

    constructor(readonly payload: number) {}
}

export class DeleteBookRequestAction implements Action {
    readonly type = BookActionTypes.DELETE_BOOK_REQUEST;

    constructor(readonly payload: number) {}
}

export class DeleteBookSuccessAction implements Action {
    readonly type = BookActionTypes.DELETE_BOOK_SUCCESS;

    constructor(readonly payload: number) {}
}

export class AddBookRequestAction implements Action {
    readonly type = BookActionTypes.ADD_BOOK_REQUEST;

    constructor(readonly payload: Book) {}
}

export class AddBookSuccessAction implements Action {
    readonly type = BookActionTypes.ADD_BOOK_SUCCESS;

    constructor(readonly payload: Book) {}
}

export class UpdateBookRequestAction implements Action {
    readonly type = BookActionTypes.UPDATE_BOOK_REQUEST;

    constructor(readonly payload: Book) {}
}

export class UpdateBookSuccessAction implements Action {
    readonly type = BookActionTypes.UPDATE_BOOK_SUCCESS;

    constructor(readonly payload: Book) {}
}


export type BookAction = LoadBooksRequestAction |
    LoadBooksFailureAction |
    LoadBooksSuccessAction |
    SelectBookAction |
    DeleteBookRequestAction |
    DeleteBookSuccessAction |
    AddBookRequestAction |
    AddBookSuccessAction |
    UpdateBookRequestAction |
    UpdateBookSuccessAction;

