import { ActionReducerMap, createSelector } from '@ngrx/store';

import { bookReducer, BookState } from './book.reducers';


export interface AppState {
    books: BookState;
}

export const selectBooks = (state: AppState) => state.books;

export const bookMap = createSelector(
    selectBooks,
    (state: BookState) => state.books
);

export const getSelectedBook = createSelector(
    selectBooks,
    (state: BookState) => state.selectedBook
);

export const reducers: ActionReducerMap<AppState> = {
    books: bookReducer
};
