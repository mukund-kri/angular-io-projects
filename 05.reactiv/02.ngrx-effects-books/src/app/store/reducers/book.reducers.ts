import { BookAction, BookActionTypes } from '../actions';
import { Book } from 'src/app/models/book.model';


export interface BookState {
    books: Map<number, Book>;
    selectedBook?: Book;
}

export const initialState: BookState = {
    books: new Map<number, Book>()
};


export function bookReducer(state: BookState = initialState, action: BookAction): BookState {
    switch (action.type) {
        case BookActionTypes.LOAD_BOOKS_SUCCESS: {
            const books = new Map(state.books);
            for (const book of action.payload) {
                books.set(book.id, book);
            }
            return {
                ...state,
                books
            };
        }
        case BookActionTypes.SELECT_BOOK: {
            const selectedBook = state.books.get(action.payload);
            return {
                ...state,
                selectedBook
            };
        }
        case BookActionTypes.DELETE_BOOK_SUCCESS: {
            const books = state.books;
            books.delete(action.payload);
            return {
                ...state,
                books
            };
        }
        case BookActionTypes.ADD_BOOK_SUCCESS: {
            const books = state.books;
            const savedBook = action.payload;
            books.set(savedBook.id, savedBook);
            return {
                ...state,
                books
            };
        }
        case BookActionTypes.UPDATE_BOOK_SUCCESS: {
            const books = state.books;
            books.delete(action.payload.id);
            return {
                ...state,
                books
            };
        }
        default: return state;
    }
}

