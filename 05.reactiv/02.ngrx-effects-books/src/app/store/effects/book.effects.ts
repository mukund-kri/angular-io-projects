import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { map, mergeMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';

import { BookService } from '../../book.service';
import {
    BookActionTypes,
    LoadBooksSuccessAction,
    DeleteBookSuccessAction,
    DeleteBookRequestAction,
    AddBookSuccessAction
} from '../actions';


@Injectable()
export class BookEffects {

    constructor(
        private bookService: BookService,
        private actions$: Actions
    ) {}

    @Effect()
    loadAllBooks$: Observable<Action> = this.actions$.pipe(
        ofType(BookActionTypes.LOAD_BOOKS_REQUEST),
        mergeMap(action =>
            this.bookService.getBooks().pipe(
                map(data => {
                    return new LoadBooksSuccessAction(data);
                })
            )
        )
    );

    @Effect()
    deleteBook$: Observable<Action> = this.actions$.pipe(
        ofType(BookActionTypes.DELETE_BOOK_REQUEST),
        mergeMap((action: any) =>
            this.bookService.deleteBook(action.payload).pipe(
                map(data => {
                    return new DeleteBookSuccessAction(action.payload);
                })
            )
        )
    );

    @Effect()
    createBook$: Observable<Action> = this.actions$.pipe(
        ofType(BookActionTypes.ADD_BOOK_REQUEST),
        mergeMap((action: any) =>
            this.bookService.createBook(action.payload).pipe(
                map(data => new AddBookSuccessAction(data)
                )
            )
        )
    );

    @Effect()
    updateBook$: Observable<Action> = this.actions$.pipe(
        ofType(BookActionTypes.UPDATE_BOOK_REQUEST),
        mergeMap((action: any) =>
            this.bookService.updateBook(action.payload).pipe(
                map(data => new AddBookSuccessAction(data))
            )
        )
    );
}
