import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Book } from '../models/book.model';
import { BookService } from '../book.service';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState, getSelectedBook } from '../store/reducers';
import { SelectBookAction } from '../store/actions';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html'
})
export class BookDetailsComponent implements OnInit {

  private book$: Observable<Book>;

  constructor(
    private bookService: BookService,
    private route: ActivatedRoute,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    const id = parseInt(this.route.snapshot.params['id'], 10);
    this.store.dispatch(new SelectBookAction(id));
    this.book$ = this.store.select(getSelectedBook);
  }

}
