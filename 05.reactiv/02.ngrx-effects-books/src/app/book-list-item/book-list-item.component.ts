import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { BookService } from '../book.service';
import { Book } from '../models/book.model';
import { AppState } from '../store/reducers';
import { DeleteBookRequestAction } from '../store/actions';


@Component({
  selector: '[app-book-list-item]',
  templateUrl: './book-list-item.component.html',
  styleUrls: ['./book-list-item.component.less']
})
export class BookListItemComponent {

  @Input() book: Book;

  constructor(
    private bookService: BookService,
    private router: Router,
    private store: Store<AppState>,
  ) { }

  deleteBook() {
    this.store.dispatch(new DeleteBookRequestAction(this.book.id));
  }

  showDetails() {
    this.router.navigate(['details', this.book.id]);
  }
}
