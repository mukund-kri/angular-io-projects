import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';

import { Book } from '../models/book.model';
import { AppState, getSelectedBook } from '../store/reducers';
import { UpdateBookRequestAction, SelectBookAction } from '../store/actions';


@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html'
})
export class BookEditComponent implements OnInit {

  private book: Book;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>,
  ) { }

  ngOnInit() {
    const id = parseInt(this.route.snapshot.params['id'], 10);
    this.store.dispatch(new SelectBookAction(id));
    this.store.select(getSelectedBook)
      .subscribe(book => {
        this.book = book;
      });
  }

  updateBook() {
    this.store.dispatch(new UpdateBookRequestAction(this.book));
    this.router.navigate(['/']);
  }
}
