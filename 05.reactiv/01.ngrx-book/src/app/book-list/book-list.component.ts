import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Book } from '../models/book.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';


@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.less']
})
export class BookListComponent implements OnInit {

  private books: Observable<Book[]>;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit() {
    this.books = this.store.select('books');
  }

}
