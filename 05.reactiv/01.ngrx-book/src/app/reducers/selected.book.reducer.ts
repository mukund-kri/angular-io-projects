import { Action } from '@ngrx/store';
import * as bookActions from '../actions/book.actions';
import { SelectedBook } from '../app.state';

const initialState: SelectedBook = { index: -1, book: null };

export function reducer(state: SelectedBook = initialState,
                        action: bookActions.Actions): SelectedBook {

  switch(action.type) {
    case bookActions.SELECTED_BOOK_SET:
      return action.payload;
    default:
      return initialState;
  }
}
