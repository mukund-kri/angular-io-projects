import { Book } from '../models/book.model';
import * as bookActions from '../actions/book.actions';


const initialState: Book[] = [
  {
    isbn: 'abcd',
    title: 'A Book',
    description: 'description of A book',
    authors: 'Author A',
  },
  {
    isbn: 'efgh',
    title: 'B Book',
    description: 'description of B book',
    authors: 'Author B'
  }
]

export function reducer(state: Book[] = initialState,
                        action: bookActions.Actions): Book[] {
  switch(action.type) {
    case bookActions.ADD_BOOK:
      return [...state, action.payload];
    case bookActions.DELETE_BOOK:
      return state.filter(book => book.isbn !== action.payload);
    default:
      return state;
  }
}
