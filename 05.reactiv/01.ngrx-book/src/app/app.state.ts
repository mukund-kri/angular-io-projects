import { Book } from './models/book.model';


export interface SelectedBook {
  index: number;
  book: Book;
}

export interface AppState {
  readonly books: Book[];
  readonly selectedBook: SelectedBook;
}
