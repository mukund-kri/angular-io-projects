import { Action } from '@ngrx/store';
import { Book } from '../models/book.model';
import { SelectedBook } from '../app.state';


export const ADD_BOOK = '[BOOK] Add';
export const DELETE_BOOK = '[BOOK] Delete';

export const SELECTED_BOOK_SET = '[SELECTED BOOK] Set';


export class AddBook implements Action {
  readonly type = ADD_BOOK;

  constructor(public payload: Book) {}
}

export class DeleteBook implements Action {
  readonly type = DELETE_BOOK;

  constructor(public payload: string) {}
}

export class SelectedBookSet implements Action {
  readonly type = SELECTED_BOOK_SET;

  constructor(public payload: SelectedBook) {}
}

export type Actions = AddBook | DeleteBook | SelectedBookSet;
