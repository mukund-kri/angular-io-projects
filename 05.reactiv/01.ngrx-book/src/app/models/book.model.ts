export interface Book {
  isbn?: string,
  title?: string,
  description?: string,
  authors?: string,
}
