import { Component, OnInit } from '@angular/core';
import { Book } from '../models/book.model';
import { Store } from '@ngrx/store';
import { AddBook } from '../actions/book.actions';
import { Router } from '@angular/router';


@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.less']
})
export class BookCreateComponent implements OnInit {

  private book: Book;

  constructor(
    private store: Store<Book[]>,
    private router: Router,
  ) { }

  ngOnInit() {
    this.book = {isbn: '', title: '', description: '', authors: ''};
  }

  onSubmit() {
    this.store.dispatch(new AddBook(this.book));
    this.router.navigate(['/']);
  }

}
