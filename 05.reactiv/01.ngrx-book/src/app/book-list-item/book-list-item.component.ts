import { Component, OnInit, Input } from '@angular/core';
import { Book } from '../models/book.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';
import * as bookActions from '../actions/book.actions';


@Component({
  selector: 'app-book-list-item',
  templateUrl: './book-list-item.component.html',
  styleUrls: ['./book-list-item.component.less']
})
export class BookListItemComponent implements OnInit {

  @Input() private book: Book;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
  }

  delete() {
    this.store.dispatch(new bookActions.DeleteBook(this.book.isbn));
  }
}
